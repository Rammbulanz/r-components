import { OnOffBehavior } from "../behaviors/on-off-behavior";
import { TemplateResult, html } from "lit-html";
import styles from './checkbox.scss.js'
import { property, customElement, query } from "lit-element";

@customElement('r-checkbox')
export class Checkbox extends OnOffBehavior {

    public static styles = styles;

    @property({ type: String, reflect: true })
    public label: string;

    @query('input#native-input')
    public nativeInput: HTMLInputElement;

    @property({ type: String, reflect: true, attribute: 'icon-checked' })
    public iconChecked: string;

    @property({ type: String, reflect: true, attribute: 'icon-unchecked' })
    public iconUnchecked: string;
    
    protected render(): TemplateResult {
        return html`
            <input id=native-input type=checkbox hidden aria-checked=${this.checked} aria-hidden=true ?checked=${this.checked} @input=${this._onNativeInput} >
            <label id="container" for=native-input>
                <i class="material-icons if-checked">check_box</i>
                <i class="material-icons if-unchecked">check_box_outline_blank</i>
                <slot>
                    <span>${this.label}</span>
                </slot>
            </label>
        `;
    }

    private _onNativeInput(): void {
        if (this.checked !== this.nativeInput.checked)
            this.checked = this.nativeInput.checked;
    }

}

declare global {
    interface HTMLElementTagNameMap {
        'r-checkbox': Checkbox
    }
}