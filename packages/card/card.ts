import { TemplateResult, html } from "lit-html";
import { customElement, LitElement } from "lit-element";
import styles from './card.scss.js'

@customElement('r-card')
export class Ripple extends LitElement {

    public static styles = styles;

    protected render(): TemplateResult {
        return html`
            <div class="card">
                <slot></slot>
            </div>            
        `;
    }

}


declare global {
    interface HTMLElementTagNameMap {
        'r-card': Ripple
    }
}