import { TemplateResult, html } from "lit-html";
import { customElement, LitElement } from "lit-element";
import styles from './ripple.scss.js'

@customElement('r-ripple')
export class Ripple extends LitElement {

    public static styles = styles;

    protected render(): TemplateResult {
        return html`
            <div class="ripple"></div>
            <slot></slot>
        `;
    }

}


declare global {
    interface HTMLElementTagNameMap {
        'r-ripple': Ripple
    }
}