import { BehaviorComponent } from "./behavior-component";
import { property, css } from "lit-element";

export abstract class OnOffBehavior extends BehaviorComponent {

    @property({ type: Boolean, reflect: true })
    public checked: boolean;

}