import { LitElement, TemplateResult, html, customElement, property } from "lit-element";
import styles from './button.scss.js'

@customElement("r-button")
export class Button extends LitElement {

    public static styles = styles;

    @property({ type: Boolean, reflect: true })
    public flat: boolean;

    @property({ type: Boolean, reflect: true })
    public outlined: boolean;

    @property({ type: Boolean, reflect: true })
    public disabled: boolean;

    protected render(): TemplateResult {
        return html`
            <button ?disabled="${this.disabled}">
                <slot name="before"></slot>
                <span class="text">
                    <slot></slot>
                </span>
                <slot name="after"></slot>
            </button>
        `;
    }

}

declare global {
    interface HTMLElementTagNameMap {
        'r-button': Button
    }
}