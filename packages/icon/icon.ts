import { LitElement, TemplateResult, html } from "lit-element";
import styles from './icon.scss.js'

export class Icon extends LitElement {

    public static styles = styles;

    protected render(): TemplateResult {
        return html`
            <slot></slot>
        `;
    }

}