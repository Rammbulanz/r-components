const gulp = require("gulp");
const tsc = require("gulp-typescript")
const sass = require("gulp-sass");
const litCss = require("@rammbulanz/gulp-tools").litStyle;
const merge = require("merge-stream");
const del = require("del");

sass.compiler = require("node-sass");

const tsProject = tsc.createProject('tsconfig.json');

// Clean

gulp.task("clean", function () {
    return del([
        "packages/**/*.css",
        "packages/**/*.js",
        "packages/**/*.scss.ts"
    ]);
})

// Build

gulp.task("build:typescript", function () {
    const ts = tsProject.src()
        .pipe(tsProject());

    const jsStream = ts.js.pipe(gulp.dest('packages'));

    return jsStream;
})

gulp.task("build:scss", function () {
    return gulp.src(['packages/**/*.scss'])
        .pipe(sass({
            includePaths: ["node_modules"]
        }))
        .pipe(gulp.dest("packages"))
        .pipe(litCss({ extension: '.ts' }))
        .pipe(gulp.dest('packages'));
});

gulp.task("build", gulp.series("clean", "build:scss", "build:typescript"));


// Watch

gulp.task("watch:typescript", function () {
    return gulp.watch(['packages/**/*.ts'], {
        usePolling: true,
        ignoreInitial: false
    }, gulp.series("build:typescript"))
});

gulp.task("watch:scss", function () {
    return gulp.watch(['packages/**/*.scss'], {
        usePolling: true,
        ignoreInitial: false
    }, gulp.series("build:scss"))
});

gulp.task("watch", gulp.parallel("watch:scss", "watch:typescript"));


gulp.task("default", gulp.series("build"));